#!/bin/bash

start(){
  echo "Testing nginx config before start ..."
  nginx -t
  echo "Starting ..."
  nginx -g "daemon off;"
}

stop(){
  echo "Stopping service ..."
  pkill nginx
  echo "Service stopped"
}

trap stop SIGINT SIGTERM SIGKILL
start $@
