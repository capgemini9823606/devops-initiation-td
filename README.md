# devops-initiation
Cette formation est une initiation au DevOPS et a pour but d'en illustrer les grands principes.
## Installation
**Initialisation :**  
Cette partie suppose que l'environnement utilisé est un OS Windows et permet d'utiliser WSL.  
```bash
# Dans un cmd windows
wsl --install
wsl -s ubuntu
```
Les étapes suivantes sont réalisées sous Ubuntu (via WSL) :  
**Sudoers sans mot de passe :**
```bash
sudo chmod +w /etc/sudoers
sudo vim /etc/sudoers
#---> %sudo   ALL=(ALL:ALL) NOPASSWD: ALL
sudo chmod -w /etc/sudoers
```
**Conteneurisation :**
```bash
sudo apt install docker
sudo apt install podman

# Kubectl
curl -LO https://dl.k8s.io/release/$(curl -Ls https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
kubectl version --client

# Kind
# For AMD64 / x86_64
[ $(uname -m) = x86_64 ] && curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.22.0/kind-linux-amd64
# For ARM64
[ $(uname -m) = aarch64 ] && curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.22.0/kind-linux-arm64
chmod +x ./kind
sudo mv ./kind /usr/local/bin/kind

# Minikube, équivalent à Kind, on ne s'en servira pas ici
#curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 \
#  && chmod +x minikube
#sudo mkdir -p /usr/local/bin/
#sudo install minikube /usr/local/bin/
#minikube start --driver=docker

```
**Jenkins :**
```bash
# Jenkins
sudo wget -O /usr/share/keyrings/jenkins-keyring.asc \
  https://pkg.jenkins.io/debian-stable/jenkins.io-2023.key
echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
  https://pkg.jenkins.io/debian-stable binary/ | sudo tee \
  /etc/apt/sources.list.d/jenkins.list > /dev/null

sudo apt-get update
sudo apt install fontconfig openjdk-17-jre
java -version
sudo apt-get install jenkins
# Si problème de démarrage (pas de systemctl)
sudo /etc/init.d/jenkins start
# Sur firefox http://localhost:8080
# renseigner le mot de passe généré dans l'interface graphique
cat /var/lib/jenkins/secrets/initialAdminPassword
# Installer les plugins de base
# Configurer un utilisateur admin
# user : admin
# pass : Admin$01
# Pour appliquer les nouveaux plugins
systemctl restart jenkins
usermod -aG docker jenkins
# Installer le plugin Docker pipeline et redémarrer
systemctl restart jenkins
```

## Configuration
**Clé ssh**
```bash
# Créer une clé ssh pour Jenkins et pour vous éviter de renseigner le mot de passe
ssh-keygen
cat ~/.ssh/id_rsa.pub
# Ajouter la clé publique dans le profil gitlab
# Profil > Edit profile > SSH Keys > Add new key
```
## Git
1. Créer un projet git
2. Clone le projet :  
  ``git clone <url git repo>``
3. Commandes de base :
    * Ajouter un fichier modifié :  
    ``git add <file_name>``
    * Commiter (enregister les modifications ajoutées précédemment) :  
    ``git commit -m "Message pertinent expliquant les modifications"``
    * Envoyer les modifications commitées dans le dépôt distant :  
    ``git push``

## Docker
1. Créer un fichier `Dockerfile` pour construire une nouvelle image Docker.
2. Cette nouvelle image Docker se base sur une image existante `nginx:latest`
3. Cette image a un entrypoint : `/entrypoint.sh`
4. Créer ce fichier entrypoint dans un nouveau dossier docker.
5. Créer 2 fonction dans l'entrypoint : `start` et `stop`
6. La fonction `start()` : 
    * Préviens de la vérification de la configuration
    * Vérifie la configuration nginx : `nginx -t`
    * Préviens du démarrage du service nginx
    * Démarre le service nginx
7. La fonction `stop()`:
    * Préviens de l'arrêt
    * Tue le processus nginx : `pkill nginx`  
7. 1. Le script lance la fonction stop sur réception d'un signal d'arrêt et lance la fonciton start.
```bash
trap stop SIGINT SIGTERM SIGKILL
start $@
```
8. Dans le dossier `docker` créer les dossiers :
    * etc/nginx/conf.d
    * usr/share/nginx/html/devops-initiation
9. Créer un fichier de configuration nginx : `docker/etc/nginx/conf.d/devops-initiation.conf`
```
server {
    listen       8082;
    server_name  localhost;

    location / {
        root   /usr/share/nginx/html/devops-initiation;
        index  index.html index.htm;
    }
    location /metrics {
        stub_status;
    }
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }
}
```
10. Créer un fichier html (à aggrémenter) : `docker/usr/share/nginx/html/devops-initiation/index.html`
```html
<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Titre de la page</title>
  <link rel="stylesheet" href="style.css">
  <script src="script.js"></script>
</head>
<body>
  Hello, welcome to devops initiation !
</body>
</html>
```
11. Copier le dossier docker dans la nouvelle image (via le `Dockerfile` et l'instruction [COPY](https://docs.docker.com/reference/dockerfile/#copy)).
12. Créer la nouvelle image : `docker build . -t localhost/devops-initiation:0.0.0`

## Docker Hub
1. Se rendre sur [Docker HUB](https://hub.docker.com/)
2. Singin with google
3. Profil > My Profile > Security > New Access Token > Generate
4. Bien copier et sauvegarder le token (en cas de perte il faudra en générer un nouveau)
5. Pousser sa première image sur Docker Hub :  
```bash
docker login -u <docker hub login>
# Entrer son token
docker tag localhost/devops-initiation:0.0.0 <docker hub login>/devops-initiation:0.0.0
docker push <docker hub login>/devops-initiation:0.0.0
```
6. Vérification : `https://hub.docker.com/repository/docker/<docker hub login>/devops-initiation/general`
7. Note : par défaut les images sont publiques, vous n'aurez pas besoin de vous logger pour les utiliser


## Kubernetes
1. Création de cluster kube avec Kind
```bash
cat <<EOF | kind create cluster --config=-
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
  kubeadmConfigPatches:
  - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "ingress-ready=true"
  extraPortMappings:
  - containerPort: 80
    hostPort: 80
    protocol: TCP
  - containerPort: 443
    hostPort: 443
    protocol: TCP
EOF
kubectl cluster-info --context kind-kind
# Retour cmd
#Kubernetes control plane is running at https://127.0.0.1:39761
#CoreDNS is running at https://127.0.0.1:39761/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml

# Vérification
kubectl get pods -A
```
2. Création d'une app dans Kube
    * Préparation
    ```bash
    mkdir kube
    cd kube
    ```
    * Créer un namespace (fichier `namespace.yml`)
    ```yaml
    apiVersion: v1
    kind: Namespace
    metadata:
      name: devops-initiation
    ```
    * Créer un deployment (fichier `deployment.yml`)
    ```yaml
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: devops-initiation-deployment
      namespace: devops-initiation
      labels:
        app: devops-initiation
    spec:
      replicas: 1
      selector:
        matchLabels:
          app: devops-initiation
      template:
        metadata:
          labels:
            app: devops-initiation
        spec:
          containers:
          - name: devops-initiation
            image: <docker hub login>/devops-initiation:0.0.0
            ports:
            - containerPort: 8082
    ```

    * Créer un service (fichier `service.yml`)
    ```yaml
    apiVersion: v1
    kind: Service
    metadata:
      name: devops-initiation-svc
      namespace: devops-initiation
    spec:
    #  type: NodePort
      selector:
    #    app.kubernetes.io/name: devops-initiation
        app: devops-initiation
      ports:
        - protocol: TCP
          port: 8082
          targetPort: 8082
    #      nodePort: 30082
    ```

    * Créer un ingress (`ingress.yml`)
    ```yaml
    apiVersion: networking.k8s.io/v1
    kind: Ingress
    metadata:
      name: devops-initiation-ingress
      namespace: devops-initiation
      annotations:
        nginx.ingress.kubernetes.io/rewrite-target: /$2
    spec:
      rules:
      - http:
          paths:
          - pathType: Prefix
            path: /devops-initiation(/|$)(.*)
            backend:
              service:
                name: devops-initiation-svc
                port:
                  number: 8082
    ```
    
    ```bash
    kubectl apply -f namespace.yml
    kubectl apply -f service.yml
    kubectl apply -f deployment.yml
    kubectl apply -f ingress.yml
    ```
    On peut aussi faire un deploiement en utilisant kustomization.yml:
    ```yaml
    apiVersion: kustomize.config.k8s.io/v1beta1
    kind: Kustomization

    resources:
      - namespace.yml
      - ingress.yml
      - service.yml
      - deployment.yml
    ```
    Pour voir le fichier généré par kustomize:
    ```bash
    kubectl kustomize . 
    ```
    
    Pour appliquer les fichiers:
    ```bash
    kubectl apply -k .
    ```
    * Vérification
    ```bash
    kubectl get pods -n devops-initiation
    # exemple de retour :
    #NAME                                            READY   STATUS    RESTARTS   AGE
    #devops-initiation-deployment-6d54796494-bz98x   1/1     Running   0          27m
    ```
    Astuce, on peut changer le context kubernetes avec la commande suivante:
    ```bash
    kubectl config set-context --current --namespace=devops-initiation
    ```

    * Test sur un navigateur : [devops-initiation](http://localhost/devops-initiation)



## Pipeline Jenkins
1. Ajouter le token Docker Hub récupéré à plus [haut](#docker-hub) dans les [secrets de Jenkins](http://localhost:8080/manage/credentials/store/system/domain/_/newCredentials)
    * Type : user/password
    * Portée : global
    * Username : <user dockerhub>
    * Password : <token dockerhub>
    * ID : `dockerhub`
2. Créer un fichier Jenkinsfile à la racine du projet.
```groovy
pipeline {
  agent any
  stages {
    stage('Test docker pipeline') {
      agent {
        docker{
          image 'ubuntu:22.04'
        }

      }
      steps {
        script {
          echo "Hello, ceci est un script lancé dans un conteneur docker."
        }
      }
    }
    stage('Build') {
      steps {
        script {
          echo "Hello, ceci est un scrip lancé hors d'un conteneur docker"
        }
      }
    }
  }
}
```
3. Créer un [pipeline Jenkins](http://localhost:8080/view/all/newJob)
    * Pipeline definition > Script from SCM > GIT
    * URL : url du git
    * Credential : clé ssh ajoutée plus [haut](#configuration)
    * Script Path : Jenkinsfile
4. Aller dans le pipeline Jenkins et lancer un build
5. Mettre à jour le stage `Build` :
    * Ajouter une variable d'environnement dans le stage (avant la partie `steps`)
    ```
    environment{
      DOCKERHUB_CREDS = credentials('dockerhub')
    }
    ```
    * Note : 2 variables sont créées automatiquement : `DOCKERHUB_CREDS_USR` et `DOCKERHUB_CREDS_PSW` (exemple pour les appeler : `${DOCKERHUB_CREDS_USR}`)
    * Ajouter dans la partie script le build de l'image, le login a Docker Hub et le push de l'image

## Helm
Installation de helm :
```bash
curl https://get.helm.sh/helm-v3.14.1-linux-amd64.tar.gz -O
tar zxvf helm-v3.14.1-linux-amd64.tar.gz
sudo mv linux-amd64/helm /usr/local/bin/helm
rm -rf linux-amd64/
helm version
# retour :
# version.BuildInfo{Version:"v3.14.1", GitCommit:"e8858f8696b144ee7c533bd9d49a353ee6c4b98d", GitTreeState:"clean", GoVersion:"go1.21.7"}
```

## Prometheus

Repo de charts: https://artifacthub.io

Nous allons installer prometheus en utilisant les helm chart. Pour cela, il faut effectuer les commandes suivantes:
```bash
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm install my_prometheus prometheus-community/prometheus
```

Il faut ensuite se rendre sur localhost:9090 pour avoir acces a la UI Prometheus.

## Ansible
**Installation :**
```bash
sudo apt install pipx
pipx install --include-deps ansible
pipx ensurepath
# Relog
ansible --version
```
**Exercice pratique :**
1. Créer un dossier `ansible`
2. ``cd ansible``
2. Créer un fichier d'inventaire : `hosts`
3. Ajouter un host `kube_server`
4. Ajouter à ce host la variable suivante :
```yaml
ansible_host: 127.0.0.1
```
5. Créer un role `devops_initiation`
```bash
mkdir roles
ansible-galaxy role init roles/devops_initiation
ls -lh roles/devops_initiation
```
6. Dans le fichier `roles/devops_initiation/tasks/main.yml` :
    1. Créer une variable `tmp_folder` (module [set_fact](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/set_fact_module.html)) avec pour valeur : `/tmp/ansible_devops_initiation`
    2. Créer le dossier temporaire (module [file](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html))
7. Copier le fichier `kube/deployment.yml` du projet dans le role comme suit :
```bash
cp ../kube/deployment.yml roles/devops_initiation/templates/deployment.yml.j2
```
8. Copier les autres fichiers du dossier kube dans `roles/devops_initiation/files`
9. Dans `roles/devops_initiation/templates/deployment.yml.j2` remplacer le nom de l'image par une variable : `"{{ image_name }}"`
10. Dans `roles/devops_initiation/templates/deployment.yml.j2` remplacer le tag de l'image par une variable : `"{{ image_tag }}"`
11. Dans l'inventaire renseigner les variables avec la bonne valeur :
```yaml
# Exemple :
image_name: "my_name/devops-initiation"
image_tag: "0.0.0"
``` 
12. Dans le fichier `roles/devops_initiation/tasks/main.yml` : 
    1. Générer un fichier deployment.yml à partir du template `roles/devops_initiation/templates/deployment.yml.j2` dans le dossier `tmp_folder` (module : [template](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html))
    1. Copier les fichiers kube à partir du dossier `roles/devops_initiation/files` dans le dossier `tmp_folder` (module [copy](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/copy_module.html))
    ```yaml
    - name: Copy kube files
      copy:
        src: "{{ item }}"
        dest: "{{ tmp_folder }}/{{ item }}"
      with_items:
        - namespace.yml
        - service.yml
        - ingress.yml
    ```
    2. Lancer la commande kubectl apply -f sur tous les fichiers kube dans l'ordre
    ```yaml
    - name: Kube apply
      command: "kubectl apply -f {{ tmp/folder }}/{{ item }}"
      with_items:
        - namespace.yml
        - deployment.yml
        - service.yml
        - ingress.yml
    ```
    3. Supprimer le dossier temporaire (module [file](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html))
13. Créer un fichier `playbook.yml`
14. Dans le fichier `playbook.yml` appeler le role `devops_initiation`
```yaml
---
- hosts: kube_server
  connection: local
  roles:
    - devops_initiation
```
15. Nettoyer le namespace :
```bash
kubectl delete ns devops-initiation
```
16. Lancer le playbook :
```bash
ansible-playbook -i hosts playbook.yml
```
17. Vérifier (navigateur utilisable également) :
```bash
kubectl get pods -n devops-initiation
```

## Jenkins X Ansible
1. Installer Ansible pour l'utilisateur Jenkins
```bash
sudo su jenkins
pipx install --include-deps ansible
# Relog
ansible --version
exit
```
2. Ajouter un nouveau stage Jenkins `deploy`
3. Ajouter le fichier .kube/config de votre utilisateur dans les [secrets de Jenkins](http://localhost:8080/manage/credentials/store/system/domain/_/newCredentials)
    * Type : secret file
    * Portée : global
    * Choisir le fichier
    * **ATTENTION**: si vous recréez le cluster kube il faudra mettre à jour ce fichier
5. Jouer la commande ansible-playbook dans le Jenkinsfile avec la configuration kube mise en secret :
```
    stage('deploy') {
      environment{
        KUBECONFIG = credentials('kube_config')
        PATH="${env.PATH}:/var/lib/jenkins/.local/bin"
      }
      steps {
        script {
          sh 'ansible-playbook -i ansible/hosts ansible/playbook.yml'
        }
      }
    }
```

# Sources
* [Kubectl](https://kubernetes.io/fr/docs/tasks/tools/install-kubectl/#install-kubectl-on-linux)  
* [Minikube](https://kubernetes.io/fr/docs/tasks/tools/install-minikube/)  
* [Helm](https://helm.sh/docs/intro/install/)  
* [Kind](https://kind.sigs.k8s.io/docs/user/ingress/) 
* [Kong](https://github.com/Kong/charts)  
* [Jenkins](https://www.jenkins.io/doc/book/installing/linux/)  
* [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)  